#ifndef MY_STRING_H
#define MY_STRING_H

#include <string>

class myString
{
public:
	void init(std::string str);
	std::string getStr();
	unsigned int getLength();
	char getCharUsingSquareBrackets(unsigned int index);
	char getCharUsingRoundBrackets(unsigned int index);
	char getFirstChar();
	char getLastChar();
	int getCharFirstOccurrenceInd(char c);
	int getCharLastOccurrenceInd(char c);
	int gstStringFirstOccurrenceInd(std::string str);
	bool isEqual(std::string s);
private:
	std::string* _string;
};

#endif MY_STRING_H