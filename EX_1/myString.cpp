#include<iostream>
#include "myString.h"

void myString::init(std::string str)
{
	this->_string = &str;
}

std::string myString::getStr()
{
	return *this->_string;
}

unsigned int myString::getLength()
{
	return this->_string->length();
}

char myString::getCharUsingSquareBrackets(unsigned int index)
{
	return (*this->_string)[index];
}

char myString::getCharUsingRoundBrackets(unsigned int index)
{
	return this->_string->at(index);
}

char myString::getFirstChar()
{
	return this->_string->at(0);
}

char myString::getLastChar()
{
	return this->_string->at(-1);
}

int myString::getCharFirstOccurrenceInd(char c)
{
	return (*this->_string).find_first_of(c);
}

int myString::getCharLastOccurrenceInd(char c)
{
	return (*this->_string).find_last_of(c);
}

int myString::gstStringFirstOccurrenceInd(std::string str)
{
	return (*this->_string).find_first_of(str);
}

bool myString::isEqual(std::string s)
{
	return (*this->_string == s);
}
