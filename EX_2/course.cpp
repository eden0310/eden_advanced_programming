#include <iostream>
#include "Course.h"

void Course::init(std::string name, unsigned int test1, unsigned int test2, unsigned int exam)
{
	this->_courseName = name;
	this->_test1 = test1;
	this->_test2 = test2;
	this->_exam = exam;
}

std::string Course::getName()
{
	return this->_courseName;
}

unsigned int* Course::getGradesList()
{
	unsigned int* gradesList = new unsigned int[3];
	gradesList[0] = this->_test1;
	gradesList[1] = this->_test2;
	gradesList[2] = this->_exam;
	return gradesList;
}

double Course::getFinalGrade()
{
	double finalGrade = ((this->_test1)*0.25) + ((this->_test2)*0.25) + ((this->_exam)*0.5);
	return finalGrade;
}
