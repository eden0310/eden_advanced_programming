#include "Student.h"
#include "Course.h"

#include <iostream>
#include <string>
#include <cstdlib>

#define MAX_STUDENTS 10

#define MENU_STUDENT_DETAILS 1
#define MENU_AVERAGE 2
#define MENU_PRINT_COURSES 3
#define MENU_EXIT 4

using std::string;
using std::cout;
using std::endl;
using std::cin;

Student* newStudent()
{
	cout << "Enter student's name: ";
	string name;
	cin >> name;

	cout << "Enter number of courses for student: ";
	unsigned int crsCount;
	cin >> crsCount;

	/* handle of courses list */
	Course* crs = nullptr;
	Course** courses = new Course*[crsCount];
	for (unsigned int i = 0; i < crsCount; i++)
	{
		/* Create new course */
		cout << "Enter Course name: ";
		string crsName;
		cin >> crsName;

		cout << "Enter first test grade: ";
		unsigned int test1;
		cin >> test1;

		cout << "Enter second test grade: ";
		unsigned int test2;
		cin >> test2;

		cout << "Enter exam grade: ";
		unsigned int exam;
		cin >> exam;

		cout << endl;

		// create course object
		crs = new Course;

		// call to init function of course object
		crs->init(crsName, test1, test2, exam);

		//Add course to list
		courses[i] = crs;
	}


	Student* student;

	// create student object
	student = new Student;

	// call to init function of student object
	student->init(name, courses, crsCount);

	// return student object
	return student;
}

int showMenu()
{
	cout << "Welcome to Magshimim! You have the following options:" << endl;
	cout << "1. Enter student deatails and courses" << endl;
	cout << "2. Calculate average grade of all courses" << endl;
	cout << "3. Show details of courses" << endl;
	cout << "4. Exit" << endl;
	cout << "Enter your choice: ";
	int option = 0;
	cin >> option;

	cout << endl;
	return option;
}

void printCourses(Course** list, unsigned int count)
{
	Course* currCourse;
	unsigned int* gradesList;

	for (unsigned int i = 0; i < count; ++i)
	{
		currCourse = list[i];

		gradesList = currCourse->getGradesList();

		cout << "Name: " << currCourse->getName() << ", test1 = " << gradesList[0] << ", test2 = " << gradesList[1]  \
			<< ", exam = " << gradesList[2] << ", Grade = " << currCourse->getFinalGrade() << endl;
	}
	cout << endl << endl;
}

int main()
{
	Student* student = nullptr;
	double avg = 0;

	/* Main Loop */

	bool toExit = false;
	while (!toExit)
	{
		switch (showMenu())
		{
		case MENU_STUDENT_DETAILS:
			if (student)
			{
				delete student;
			}
			student = newStudent();
			break;

		case MENU_AVERAGE:
			if (!student)
			{
				cout << "No details were added" << endl;
			}
			else
			{
				cout << "The average grade of " << student->getCrsCount() << " courses is: " << student->getAvg() << endl << endl;
			}
			break;

		case MENU_PRINT_COURSES:
			if (!student)
			{
				cout << "No details were added" << endl;
			}
			else
			{
				cout << "Courses list for student " << student->getName() << endl;
				printCourses(student->getCourses, student->getCrsCount);
			}
			break;

		case MENU_EXIT:
			toExit = true;
			cout << "Goodbye!" << endl;
			break;

		default:
			cout << "No such option!" << endl << endl;
			break;
		}
	}

	if (student)
	{
		delete student;
	}

	system("pause");
	return 0;
}