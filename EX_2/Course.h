#pragma once

#include <string>

/* Representation of a Course grade-book
test - 25%
final exam - 50%
can only change grades after creation.
*/

class Course
{
public:
	void init(std::string name, unsigned int test1, unsigned int test2, unsigned int exam);

	unsigned int* getGradesList();
	std::string getName();
	double getFinalGrade();

private:
	// TODO: What fields do you need?
	std::string _courseName;
	unsigned int _test1, _test2, _exam;
};