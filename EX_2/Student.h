#ifndef STUDENT_H
#define STUDENT_H

#include "Course.h"
#include <string>

/* Represents a student's Courses and total average
MAGIC_BONUS is: 1 for any Course that ends with 'y'
2 for any Course that starts with 'e'
5 for any Course that ends with 'h'
*/

class Student
{
public:
	void init(std::string name, Course** courses, unsigned int crsCount);

	std::string getName();
	void setName(std::string name);
	unsigned int getCrsCount();
	Course** getCourses();
	double getAvg();

private:
	std::string _name;
	Course** _courses;
	unsigned int _crsCount;
};

#endif // STUDENT_H